﻿using System;

namespace GR1TEAM01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This poject is made by Sing and Vincent");
            Console.WriteLine("Hi, this is Sing Graajae Cho");
        }

        public static string NameAndCity(string name, string city)
        {
            return $"{name} lives in {city}";
        }
    }
}
